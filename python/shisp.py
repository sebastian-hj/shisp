from utilities import Utils, PlotUtils
from shisp_class import RawInfo, CfaPattern, CfaAlignment, OffsetAlignment, NoiseReduction, WhiteBalance, Demosaic, \
    ColorCorrectionMatrix, Gamma
import matplotlib.pyplot as plt
import numpy as np


if __name__ == '__main__':
    # Set file information
    # file_path = r'test.raw'
    # bit_depth = 12
    # endianness = 'be'
    # width = 2592
    # height = 1944
    # cfa_pattern = CfaPattern.gbrg

    file_path = r'test2_1920x1080_12bit_le.raw'
    bit_depth = 12
    endianness = 'le'
    width = 1920
    height = 1080
    cfa_pattern = CfaPattern.grbg

    # Start Raw Info class
    raw_info = RawInfo()
    raw_info.width = width
    raw_info.height = height
    raw_info.cfa_pattern = cfa_pattern
    raw_info.endianness = endianness
    raw_info.bit_depth = bit_depth
    raw_info.max_pixel_value = (1 << bit_depth) - 1

    # Start utilities class
    utils = Utils()
    utils.file_path = file_path
    utils.bit_depth = bit_depth
    utils.endianness = endianness
    # Start plot class
    plot_utils = PlotUtils(raw_info)

    # Read raw file
    raw = utils.read_binary_file_to_array(raw_info.width, raw_info.height)
    # Plot raw
    plot_utils.plot_raw(raw, 'Test Raw')

    # Start CFA Alignment class
    cfa_alignment = CfaAlignment(raw_info)
    # Plot channels
    plot_utils.plot_color_channels(cfa_alignment.get_color_channels_from_raw(raw), 'Raw: Color Channels')

    # Start Offset Alignment class
    offset_alignment = OffsetAlignment(raw_info)
    # Set offset
    # offset = np.min(raw)
    offset = 168
    color_channels_offset = {'r': offset,
                             'gr': offset,
                             'gb': offset,
                             'b': offset}
    # Align offsets
    raw = offset_alignment.align_offset(raw, color_channels_offset)
    # Plot channels
    plot_utils.plot_color_channels(cfa_alignment.get_color_channels_from_raw(raw),
                                   'Offset Alignment: {offset}'.format(offset=color_channels_offset))

    # Noise Reduction
    nr = NoiseReduction(raw_info)
    # Apply noise reduction
    raw_average = nr.noise_reduction(raw, 'average', kernel_size=7)
    plot_utils.plot_raw(raw_average, 'Noise Reduction: \'avearge\'')
    # Apply noise reduction
    raw = nr.noise_reduction(raw, 'average_improved')
    plot_utils.plot_raw(raw, 'Noise Reduction: \'average improved\'')

    # White Balance
    wb = WhiteBalance(raw_info)
    # Copy array before white balance
    raw_no_wb = np.copy(raw)
    # Apply white balance
    raw = wb.white_balance(raw, 'gray')

    # Demosaic
    demosaic = Demosaic(raw_info)

    # Do demosaic (before white balance)
    rgb_no_wb = demosaic.demosaic(raw_no_wb)
    # Plot image (normalized)
    rgb_no_wb_norm = (rgb_no_wb / (1 << bit_depth) * 255.0).astype(dtype=np.uint8)
    plot_utils.plot_rgb(rgb_no_wb_norm, 'Before White Balance')

    # Do demosaic (after white balance)
    rgb = demosaic.demosaic(raw)
    # Plot image (normalized)
    rgb_norm = (rgb / (1 << bit_depth) * 255.0).astype(dtype=np.uint8)
    plot_utils.plot_rgb(rgb_norm, 'After White Balance')

    # Color Correction Matrix (CCM)
    ccm = ColorCorrectionMatrix(raw_info)
    ccm_coeff = np.array([[+1.2, -0.1, -0.1],
                          [-0.2, +1.4, -0.2],
                          [-0.1, -0.2, +1.3]])
    rgb = ccm.apply_ccm_to_rgb(rgb, ccm_coeff)
    # Plot image (normalized)
    rgb_norm = (rgb / (1 << bit_depth) * 255.0).astype(dtype=np.uint8)
    plot_utils.plot_rgb(rgb_norm, 'CCM')

    # Apply Gamma
    gamma = Gamma(raw_info)
    rgb = gamma.apply_gamma_to_rgb(rgb, 2.2)
    # Plot image (normalized)
    rgb_norm = (rgb / (1 << bit_depth) * 255.0).astype(dtype=np.uint8)
    plot_utils.plot_rgb(rgb_norm, 'Gamma')

    plt.show()
