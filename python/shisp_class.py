from enum import Enum
import numpy as np
from scipy import signal
from utilities import PlotUtils


class CfaPattern(Enum):
    rggb = 0
    grbg = 1
    gbrg = 2
    bggr = 3


class RawInfo:
    def __init__(self):
        # Public
        self.width = 0
        self.height = 0
        self.cfa_pattern = CfaPattern.rggb
        self.endianness = 'le'
        self.bit_depth = 0
        self.max_pixel_value = 0


class CfaAlignment:
    def __init__(self, raw_info):
        # Private
        self.width = raw_info.width
        self.height = raw_info.height
        self.cfa_pattern = raw_info.cfa_pattern

    def get_color_channels_from_raw(self, raw):
        # Set CFA pattern
        cfa_pattern = self.cfa_pattern

        # Set possible color channels
        r = np.empty((np.int(self.height / 2), np.int(self.width / 2)))
        gr = np.empty((np.int(self.height / 2), np.int(self.width / 2)))
        gb = np.empty((np.int(self.height / 2), np.int(self.width / 2)))
        b = np.empty((np.int(self.height / 2), np.int(self.width / 2)))

        if cfa_pattern == CfaPattern.rggb:
            #     |  0 |  1 |
            # | 0 |  R | Gr |
            # | 1 | Gb |  B |
            r = raw[0::2, 0::2]
            gr = raw[0::2, 1::2]
            gb = raw[1::2, 0::2]
            b = raw[1::2, 1::2]
        elif cfa_pattern == CfaPattern.grbg:
            #     |  0 |  1 |
            # | 0 | Gr |  R |
            # | 1 |  B | Gb |
            gr = raw[0::2, 0::2]
            r = raw[0::2, 1::2]
            b = raw[1::2, 0::2]
            gb = raw[1::2, 1::2]
        elif cfa_pattern == CfaPattern.gbrg:
            #     |  0 |  1 |
            # | 0 | Gb |  B |
            # | 1 |  R | Gr |
            gb = raw[0::2, 0::2]
            b = raw[0::2, 1::2]
            r = raw[1::2, 0::2]
            gr = raw[1::2, 1::2]
        elif cfa_pattern == CfaPattern.bggr:
            #     |  0 |  1 |
            # | 0 |  B | Gb |
            # | 1 | Gr |  R |
            b = raw[0::2, 0::2]
            gb = raw[0::2, 1::2]
            gr = raw[1::2, 0::2]
            r = raw[1::2, 1::2]
        elif cfa_pattern == CfaPattern.rgbir:
            #     |  0 |  1 |  2 |  3 |
            # | 0 |  G |  B |  G |  R |
            # | 1 | IR |  G | IR |  G |
            # | 2 |  G |  R |  G |  B |
            # | 3 | IR |  G | IR |  G |
            print('TBA...')
        else:
            print('ERROR: Unsupported CFA pattern!')

        # Create output dictionary
        color_channels = {'r': r,
                          'gr': gr,
                          'gb': gb,
                          'b': b}

        return color_channels

    def get_color_channel_in_raw(self, raw):
        # Set CFA pattern
        cfa_pattern = self.cfa_pattern

        # Create array
        raw_r = np.zeros((self.height, self.width))
        raw_g = np.zeros((self.height, self.width))
        raw_b = np.zeros((self.height, self.width))

        if cfa_pattern == CfaPattern.rggb:
            #     |  0 |  1 |
            # | 0 |  R | Gr |
            # | 1 | Gb |  B |
            raw_r[0::2, 0::2] = raw[0::2, 0::2]
            raw_g[0::2, 1::2] = raw[0::2, 1::2]
            raw_g[1::2, 0::2] = raw[1::2, 0::2]
            raw_b[1::2, 1::2] = raw[1::2, 1::2]
        elif cfa_pattern == CfaPattern.grbg:
            #     |  0 |  1 |
            # | 0 | Gr |  R |
            # | 1 |  B | Gb |
            raw_g[0::2, 0::2] = raw[0::2, 0::2]
            raw_r[0::2, 1::2] = raw[0::2, 1::2]
            raw_b[1::2, 0::2] = raw[1::2, 0::2]
            raw_g[1::2, 1::2] = raw[1::2, 1::2]
        elif cfa_pattern == CfaPattern.gbrg:
            #     |  0 |  1 |
            # | 0 | Gb |  B |
            # | 1 |  R | Gr |
            raw_g[0::2, 0::2] = raw[0::2, 0::2]
            raw_b[0::2, 1::2] = raw[0::2, 1::2]
            raw_r[1::2, 0::2] = raw[1::2, 0::2]
            raw_g[1::2, 1::2] = raw[1::2, 1::2]
        elif cfa_pattern == CfaPattern.bggr:
            #     |  0 |  1 |
            # | 0 |  B | Gb |
            # | 1 | Gr |  R |
            raw_b[0::2, 0::2] = raw[0::2, 0::2]
            raw_g[0::2, 1::2] = raw[0::2, 1::2]
            raw_g[1::2, 0::2] = raw[1::2, 0::2]
            raw_r[1::2, 1::2] = raw[1::2, 1::2]
        else:
            print('ERROR: Unsupported CFA pattern!')

        return raw_r, raw_g, raw_b

    def set_color_channels_to_raw(self, color_channels):
        # Set CFA pattern
        cfa_pattern = self.cfa_pattern

        # Create array
        raw = np.zeros((self.height, self.width))

        # Get color channels
        r = color_channels['r']
        gr = color_channels['gr']
        gb = color_channels['gb']
        b = color_channels['b']

        if cfa_pattern == CfaPattern.rggb:
            #     |  0 |  1 |
            # | 0 |  R | Gr |
            # | 1 | Gb |  B |
            raw[0::2, 0::2] = r
            raw[0::2, 1::2] = gr
            raw[1::2, 0::2] = gb
            raw[1::2, 1::2] = b
        elif cfa_pattern == CfaPattern.grbg:
            #     |  0 |  1 |
            # | 0 | Gr |  R |
            # | 1 |  B | Gb |
            raw[0::2, 0::2] = gr
            raw[0::2, 1::2] = r
            raw[1::2, 0::2] = b
            raw[1::2, 1::2] = gb
        elif cfa_pattern == CfaPattern.gbrg:
            #     |  0 |  1 |
            # | 0 | Gb |  B |
            # | 1 |  R | Gr |
            raw[0::2, 0::2] = gb
            raw[0::2, 1::2] = b
            raw[1::2, 0::2] = r
            raw[1::2, 1::2] = gr
        elif cfa_pattern == CfaPattern.bggr:
            #     |  0 |  1 |
            # | 0 |  B | Gb |
            # | 1 | Gr |  R |
            raw[0::2, 0::2] = b
            raw[0::2, 1::2] = gb
            raw[1::2, 0::2] = gr
            raw[1::2, 1::2] = r
        else:
            print('ERROR: Unsupported CFA pattern!')

        return raw


class OffsetAlignment:
    def __init__(self, raw_info):
        # Private
        self.raw_info = raw_info

    def align_offset(self, raw, color_channels_offset):
        # Get CFA alignment information
        cfa_align = CfaAlignment(self.raw_info)

        # Get color channels
        color_channels = cfa_align.get_color_channels_from_raw(raw)

        # Iterate over color channels
        for channel_name, current_channel in color_channels.items():
            if channel_name not in color_channels_offset:
                print('WARNING: No \'{channel}\' channel in offset.'.format(channel=channel_name))
            else:
                # Get current color channel with offset removed
                channel_offset_removed = current_channel - color_channels_offset[channel_name]
                # Check if any pixel value is negative
                if np.any(channel_offset_removed[channel_offset_removed < 0]):
                    print('WARNING: offset alignment for \'{channel}\' produced negative pixel values. '
                          'Clipping to zero.'.format(channel=channel_name))
                    channel_offset_removed[channel_offset_removed < 0] = 0
                # Replace color channel values
                color_channels[channel_name] = channel_offset_removed

        # Convert back to raw
        raw = cfa_align.set_color_channels_to_raw(color_channels)

        return raw


class NoiseReduction:
    """
    J. C. Russ, "Correcting Imaging Defects," in The Image Processing Handbook, sixth ed. Boca Raton, FL, USA: CRC
    Press, 2011, ch. 4, pp. 205-214
    """
    def __init__(self, raw_info):
        # Private
        self.raw_info = raw_info

    def noise_reduction(self, raw, algorithm='average', **kwargs):
        # Get CFA alignment information
        cfa_align = CfaAlignment(self.raw_info)

        # Get color channels
        color_channels = cfa_align.get_color_channels_from_raw(raw)

        # Select algorithm
        if algorithm == 'average':
            # Get/define kernel size
            if 'kernel_size' not in kwargs:
                print('WARNING: Undefined \'kernel_size\' size for algorithm \'{algo}\'. Default to \'3\''.format(algo=algorithm))
                kernel_size = 3
            else:
                kernel_size = kwargs['kernel_size']
            # Apply 'average' noise reduction
            color_channels = self.nr_average(color_channels, kernel_size)

        elif algorithm == 'average_improved':
            # Apply 'average improved' noise reduction
            color_channels = self.nr_average_improved(color_channels)

        else:
            print('WARNING: Unsupported algorithm \'{algo}\''.format(algo=algorithm))

        # Convert back to raw
        raw = cfa_align.set_color_channels_to_raw(color_channels)

        return raw

    @staticmethod
    def nr_average(color_channels, kernel_size):
        # Define kernel
        kernel = np.ones((kernel_size, kernel_size))
        kernel = kernel / np.sum(kernel)

        # Apply kernel per color channel
        color_channels['r'] = signal.convolve2d(color_channels['r'], kernel, boundary='symm', mode='same')
        color_channels['gr'] = signal.convolve2d(color_channels['gr'], kernel, boundary='symm', mode='same')
        color_channels['gb'] = signal.convolve2d(color_channels['gb'], kernel, boundary='symm', mode='same')
        color_channels['b'] = signal.convolve2d(color_channels['b'], kernel, boundary='symm', mode='same')

        return color_channels

    @staticmethod
    def nr_average_improved(color_channels):
        # Define kernel
        kernel = np.array([[1, 2, 1],
                           [2, 4, 2],
                           [1, 2, 1]]).astype(dtype=np.float)
        kernel = kernel / np.sum(kernel)

        # Apply kernel per color channel
        color_channels['r'] = signal.convolve2d(color_channels['r'], kernel, boundary='symm', mode='same')
        color_channels['gr'] = signal.convolve2d(color_channels['gr'], kernel, boundary='symm', mode='same')
        color_channels['gb'] = signal.convolve2d(color_channels['gb'], kernel, boundary='symm', mode='same')
        color_channels['b'] = signal.convolve2d(color_channels['b'], kernel, boundary='symm', mode='same')

        return color_channels


class WhiteBalance:
    """
    E. Y. Lam and G. S. K. Fung, "Automatic White Balancing in Digital Photography," in Single-Sensor Imaging: Methods
    and Applications for Digital Cameras, R. Lukac, Ed., Boca Raton, FL, USA: CRC Press, 2009, pp. 280-281
    """
    def __init__(self, raw_info):
        # Private
        self.raw_info = raw_info

    def white_balance(self, raw, algorithm='gray'):
        # Get CFA alignment information
        cfa_align = CfaAlignment(self.raw_info)

        # Get color channels
        color_channels = cfa_align.get_color_channels_from_raw(raw)

        if algorithm == 'gray':
            wb_gains = self.wb_gray_world(color_channels)
        else:
            print('WARNING: Unsupported algorithm \'{algo}\''.format(algo=algorithm))
            # Set WB gains to unity
            wb_gains = np.array([1.0, 1.0, 1.0, 1.0])

        # Apply gains per-color channel
        color_channels['r'] = color_channels['r'] * wb_gains['r']
        color_channels['gr'] = color_channels['gr'] * wb_gains['gr']
        color_channels['gb'] = color_channels['gb'] * wb_gains['gb']
        color_channels['b'] = color_channels['b'] * wb_gains['b']

        # Convert back to raw
        raw = cfa_align.set_color_channels_to_raw(color_channels)

        return raw

    @staticmethod
    def wb_gray_world(color_channels):
        # Get average per color channel
        r_avg = np.mean(color_channels['r'])
        g_avg = (np.mean(color_channels['gr']) + np.mean(color_channels['gb'])) / 2.0
        b_avg = np.mean(color_channels['b'])

        # Calculate average color ratios
        g_r_ratio = g_avg / r_avg
        g_b_ratio = g_avg / b_avg

        # Get WB gains
        wb_gains = {'r': g_r_ratio,
                    'gr': 1.0,
                    'gb': 1.0,
                    'b': g_b_ratio}

        return wb_gains


class Demosaic:
    """
    O. Losson, L. Macaire, and Y. Yan, "Comparison of color demosaicing methods," in Advances in Imaging and Electron
    Physics, vol. 162, 2010, 162, pp. 173-265.
    """
    def __init__(self, raw_info):
        # Private
        self.raw_info = raw_info

    def demosaic(self, raw):
        # Define red and blue kernels
        h_rb = (1 / 4.) * np.array([[1, 2, 1],
                                    [2, 4, 2],
                                    [1, 2, 1]]).astype(dtype=np.float)
        # Define green kernel
        h_g = (1 / 4.) * np.array([[0, 1, 0],
                                   [1, 4, 1],
                                   [0, 1, 0]]).astype(dtype=np.float)

        # Get color channels in raw
        cfa_align = CfaAlignment(self.raw_info)
        raw_r, raw_g, raw_b = cfa_align.get_color_channel_in_raw(raw)

        # plot_utils = PlotUtils(self.raw_info)
        # plot_utils.plot_raw(raw_r, 'Raw R')
        # plot_utils.plot_raw(raw_g, 'Raw G')
        # plot_utils.plot_raw(raw_b, 'Raw B')

        # Get red, green, and blue demosaic'd channels
        r = signal.convolve2d(raw_r, h_rb, boundary='symm', mode='same')
        g = signal.convolve2d(raw_g, h_g, boundary='symm', mode='same')
        b = signal.convolve2d(raw_b, h_rb, boundary='symm', mode='same')

        # Stack arrays
        rgb = np.dstack((r, g, b))

        # Clip values to maximum pixel value
        rgb = np.clip(rgb, 0, self.raw_info.max_pixel_value)

        return rgb


class ColorCorrectionMatrix:
    """
    J. E. Adams, Jr. and J. F. Hamilton, Jr., "Digital Camera Image Processing Chain Design," in Single-Sensor Imaging:
    Methods and Applications for Digital Cameras, R. Lukac, Ed., Boca Raton, FL, USA: CRC Press, 2009, pp. 77-78
    """
    def __init__(self, raw_info):
        # Private
        self.raw_info = raw_info

    @staticmethod
    def apply_ccm_to_pixel(pixel, ccm):
        return np.dot(ccm, pixel)

    def apply_ccm_to_rgb(self, rgb, ccm):
        # Apply CCM to each RGB pixel
        rgb = np.apply_along_axis(self.apply_ccm_to_pixel, 2, rgb, ccm)

        return rgb


class Gamma:
    def __init__(self, raw_info):
        # Private
        self.raw_info = raw_info

    def apply_gamma_to_rgb(self, rgb, coefficient=2.2):
        # Generate Gamma multiplier
        gamma_mult = 1.0 / coefficient

        # Normalize to 1.0
        rgb = rgb / self.raw_info.max_pixel_value

        # Apply multiplier
        rgb = np.power(rgb, gamma_mult)

        # Scale back to bit depth
        rgb = rgb * self.raw_info.max_pixel_value

        return rgb
