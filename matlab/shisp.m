
% Set file information
filePath = "test2_1920x1080_12bit_le.raw";
bitDepth = 12;
endianness = "le";
width = 1920;
height = 1080;
cfaPattern = CfaPattern.grbg;

% Start raw info struct
rawInfo = struct("width", width, ...
                 "height", height, ...
                 "cfaPattern", cfaPattern, ...
                 "endianness", endianness, ...
                 "bitDepth", bitDepth, ...
                 "maxPixelValue", bitshift(1, bitDepth) - 1);

% Start utilities class
utils = Utilities(filePath, bitDepth, endianness);
% Start plot class
plotUtils = PlotUtilities(rawInfo);

% Read raw file
raw = utils.readBinaryFileToArray(width, height);
% Plot raw
plotUtils.plotRaw(raw, "Test Raw");

% Start CFA Alignment class
cfaAlignment = CfaAlignment(rawInfo);
% Plot channels
plotUtils.plotColorChannels(cfaAlignment.getColorChannelsFromRaw(raw), "Raw: Color Channels");
