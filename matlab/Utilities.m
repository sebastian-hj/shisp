classdef Utilities
    % Utilities    Collection of utilities
    %
    % Utilities Properties:
    %   filePath - File path
    %   bitDepth - Pixel bit depth
    %   endianness - Endianness
    %
    % Utilities Methods:
    %   readBinaryFileToArray - Read binary file to array
    %   writeArrayToBinaryFile - Write array to binary file
    
    properties (Access = public)
        filePath;
        bitDepth;
        endianness;
    end
    
    methods (Access = public)
        function obj = Utilities(filePath, bitDepth, endianness)
            % Utilities Construct an instance of this class
            obj.filePath = filePath;
            obj.bitDepth = bitDepth;
            obj.endianness = endianness;
        end


        function outData = readBinaryFileToArray(obj, width, height)
            % readBinaryFileToArray Read binary file to array
            %   outData = readBinaryFileToArray(width, height)
            
            % Get endianness and data type formats
            [endiannessFormat, dataTypeFormat] = obj.getDataFormat();
            
            % Open file as read-only binary
            fileId = fopen(obj.filePath, "rb");
            
            % Read binary file based on data type and endianness formats
            outData = fread(fileId, strcat(dataTypeFormat, "=>", dataTypeFormat), ...
                            endiannessFormat);

            % Close file
            fclose(fileId);

            % Convert to double type
            outData = double(outData);

            % Resize according to image width and height
            outData = transpose(reshape(outData, [width, height]));
        end


        function writeArrayToBinaryFile(obj, outPath, array)
            %WRITEARRAYTOBINARYFILE Write array to binary file

            % Get endianness and data type formats
            [endiannessFormat, dataTypeFormat] = obj.getDataFormat();

            % Open file as write-only binary
            fileId = fopen(outPath, "wb");
            
            % Write transposed array to binary file
            fwrite(fileId, transpose(array), dataTypeFormat, endiannessFormat);
            
            % Close file
            fclose(fileId);
        end
    end


    methods (Access = private)
        function [endiannessFormat, dataTypeFormat] = getDataFormat(obj)
            %GETDATAFORMAT Get data format
            %   Get endianness and packed bit depth

            if obj.endianness == "le"
                endiannessFormat = "ieee-le";
            elseif obj.endianness == "be"
                endiannessFormat = "ieee-be";
            else
                error("Unsupported endianness '%s'. Options are 'le' or 'be'.", obj.endianness);
            end
            
            if obj.bitDepth <= 8
                dataTypeFormat = "uint8";
            elseif obj.bitDepth <= 16
                dataTypeFormat = "uint16";
            elseif obj.bitDepth <= 32
                dataTypeFormat = "uint32";
            else
                error("Unsupported bit depth '%d'.", obj.bitDepth);
            end
        end
    end
end

