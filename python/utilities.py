import numpy as np
import matplotlib.pyplot as plt


class Utils:
    def __init__(self):
        self.file_path = ''
        self.bit_depth = 12
        self.endianness = 'be'

    #
    # File I/O
    #
    def get_data_format(self):
        # Set endianness type according to input
        if self.endianness == 'le':
            endianness_format = '<'
        elif self.endianness == 'be':
            endianness_format = '>'
        else:
            print('ERROR: unsupported endianness \'{endianness}\'. Options are \'le\' or \'be\''
                  .format(endianness=self.endianness))
            return -1

        # Set data type and length factor according to input bit depth
        if self.bit_depth <= 8:
            data_type_format = 'B'
        elif 8 < self.bit_depth <= 16:
            data_type_format = 'H'
        elif 16 < self.bit_depth <= 32:
            data_type_format = 'I'
        else:
            print('ERROR: unsupported bit depth \'{bit_depth}\''.format(bit_depth=self.bit_depth))
            return -1

        # Construct binary data format
        bin_data_format = endianness_format + data_type_format

        return bin_data_format

    def read_binary_file_to_array(self, width, height):
        # Get binary data format
        bin_data_format = self.get_data_format()
        if bin_data_format == -1:
            print('ERROR: Unable to set binary data format')
            return -1

        try:
            out_data = np.fromfile(self.file_path, dtype=bin_data_format)
        except IOError:
            print('ERROR opening file \'{file_path}\'.'.format(file_path=self.file_path))
            return -1

        # Resize according to image width and height and convert to float
        out_data = np.reshape(out_data, (height, width)).astype(dtype=np.float)

        return out_data

    def write_array_to_binary_file(self, out_path, array):
        # Get binary data format
        bin_data_format = self.get_data_format()
        if bin_data_format == -1:
            print('ERROR: Unable to set binary data format')
            return -1

        # Write to binary array
        array.astype(bin_data_format).tofile(out_path)


#
# Plots
#
class PlotUtils:
    def __init__(self, raw_info):
        # Public
        self.cfa_pattern = raw_info.cfa_pattern
        # Private
        # Set default figure width and height
        self.figsize_width = 9
        self.figsize_height = 6
        # Set 'dark mode'
        plt.style.use('dark_background')

    def plot_raw(self, raw, title):
        # Start figure
        f = plt.figure(figsize=(self.figsize_width, self.figsize_height))
        f.suptitle(title)

        # Add axes
        ax_raw = f.add_subplot(1, 1, 1)
        # Plot
        im_raw = ax_raw.imshow(raw, cmap='gray')
        # Add color bar
        plt.colorbar(im_raw, ax=ax_raw)
        # Set titles
        ax_raw.title.set_text('Raw')

    def plot_rgb(self, rgb, title):
        # Start figure
        f = plt.figure(figsize=(self.figsize_width, self.figsize_height))
        f.suptitle(title)

        # Add axes
        ax_rgb = f.add_subplot(1, 1, 1)
        # Plot
        im_rgb = ax_rgb.imshow(rgb)
        # Set titles
        ax_rgb.title.set_text('RGB')

    def plot_color_channels(self, color_channels, title):
        # Start figure
        f = plt.figure(figsize=(self.figsize_width, self.figsize_height))
        f.suptitle(title)

        # Add color axes
        ax_r = f.add_subplot(2, 2, 1)
        ax_gr = f.add_subplot(2, 2, 2, sharex=ax_r, sharey=ax_r)
        ax_gb = f.add_subplot(2, 2, 3, sharex=ax_r, sharey=ax_r)
        ax_b = f.add_subplot(2, 2, 4, sharex=ax_r, sharey=ax_r)

        # Plot color channels
        im_r = ax_r.imshow(color_channels['r'], cmap='gray')
        im_gr = ax_gr.imshow(color_channels['gr'], cmap='gray')
        im_gb = ax_gb.imshow(color_channels['gb'], cmap='gray')
        im_b = ax_b.imshow(color_channels['b'], cmap='gray')

        # Add color bars
        plt.colorbar(im_r, ax=ax_r)
        plt.colorbar(im_gr, ax=ax_gr)
        plt.colorbar(im_gb, ax=ax_gb)
        plt.colorbar(im_b, ax=ax_b)

        # Set titles
        ax_r.title.set_text('R')
        ax_gr.title.set_text('Gr')
        ax_gb.title.set_text('Gb')
        ax_b.title.set_text('B')
