classdef CfaAlignment
    % CfaAlignment Color Filter Array (CFA) alignment
    %   Detailed explanation goes here
    
    properties (Access = private)
        width;
        height;
        cfaPattern;
    end
    
    methods (Access = public)
        function obj = CfaAlignment(rawInfo)
            %CFAALIGNMENT Construct an instance of this class

            obj.width = rawInfo.width;
            obj.height = rawInfo.height;
            obj.cfaPattern = rawInfo.cfaPattern;
        end
        
        function colorChannels = getColorChannelsFromRaw(obj, raw)
            % getColorChannelsFromRaw Get color channels from raw image
            %   Detailed explanation goes here

            switch obj.cfaPattern
                case CfaPattern.rggb
                    %     |  0 |  1 |
                    % | 0 |  R | Gr |
                    % | 1 | Gb |  B |
                    r = raw(1:2:end, 1:2:end);
                    gr = raw(1:2:end, 2:2:end);
                    gb = raw(2:2:end, 1:2:end);
                    b = raw(2:2:end, 1:2:end);

                case CfaPattern.grbg
                    %     |  0 |  1 |
                    % | 0 | Gr |  R |
                    % | 1 |  B | Gb |
                    gr = raw(1:2:end, 1:2:end);
                    r = raw(1:2:end, 2:2:end);
                    b = raw(2:2:end, 1:2:end);
                    gb = raw(2:2:end, 1:2:end);

                case CfaPattern.gbrg
                    %     |  0 |  1 |
                    % | 0 | Gb |  R |
                    % | 1 |  B | Gb |
                    gb = raw(1:2:end, 1:2:end);
                    r = raw(1:2:end, 2:2:end);
                    b = raw(2:2:end, 1:2:end);
                    gb = raw(2:2:end, 1:2:end);

                case CfaPattern.bggr
                    %     |  0 |  1 |
                    % | 0 |  B | Gb |
                    % | 1 | Gr |  R |
                    b = raw(1:2:end, 1:2:end);
                    gb = raw(1:2:end, 2:2:end);
                    gr = raw(2:2:end, 1:2:end);
                    r = raw(2:2:end, 1:2:end);

                otherwise
                    error("Unsupported CFA pattern!");
            end
            
            % Create output struct
            colorChannels = struct("r", r, ...
                                   "gr", gr, ...
                                   "gb", gb, ...
                                   "b", b);
        end
    end
end

