# Introduction

SimpleH ISP (Image Signal Processing) pipeline in Python and MATLAB (C, C++ in the future...)

## Python

### Version
Tested on Python 3.7.4 and 2.7.16

### Utilities
#### File I/O
- Read raw/binary file to Numpy array
- Write Numpy array to binary file

#### Plot
- Plot raw, RGB
- Plot color channels

### Color Filter Array (CFA) Alignment
- Get color channels based on CFA type.
    - Bayer: RGGB, GRBG, GBRG, BGGR

### Offset Alignment
- Based on CFA and offset value, align color channels.

### Noise Reduction
- Apply 'average' noise reduction (for now) to raw image. 

### White Balance
- White balance raw image using (for now) gray world algorithm.

### Demosaic
- Demosaic raw image to RGB using (for now) bilinear algorithm.

### Color Correction Matrix
- Color correction matrix in RGB domain.

### Gamma Correction
-  Apply Gamma correction using (for now) single value to RGB image.

## MATLAB

### Version
Tested on MATLAB R2020a

### Utilities
#### File I/O
- Read raw/binary file to array
- Write array to binary file

#### Plot
- Plot raw
- Plot color channels

### Color Filter Array (CFA) Alignment
- Get color channels based on CFA type.
    - Bayer: RGGB, GRBG, GBRG, BGGR
